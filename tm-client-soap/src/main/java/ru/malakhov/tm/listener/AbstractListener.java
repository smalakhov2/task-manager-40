package ru.malakhov.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.service.SessionService;

@Component
@NoArgsConstructor
public abstract class AbstractListener {

    @Autowired
    protected SessionService sessionService;

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void handle(final ConsoleEvent event) throws Exception;

}