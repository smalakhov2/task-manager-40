package ru.malakhov.tm.exception.user;

import ru.malakhov.tm.exception.AbstractException;

public final class InvalidLoginException extends AbstractException {

    public InvalidLoginException() {
        super("Error! login is invalid...");
    }

}
