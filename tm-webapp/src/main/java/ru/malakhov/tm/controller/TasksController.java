package ru.malakhov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.dto.CustomUser;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.api.service.ITaskService;

import java.util.List;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal CustomUser user) {
        final List<Task> tasks = taskService.findAllByUserId(user.getUserId());
        return new ModelAndView("task/task-list", "tasks", tasks);
    }

}