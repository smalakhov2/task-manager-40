package ru.malakhov.tm.event;

import lombok.Getter;

@Getter
public class ConsoleEvent {

    private final String command;

    public ConsoleEvent(String command) {
        this.command = command;
    }

}