package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.TaskDTO;

import java.util.List;

@Repository
public interface ITaskRepositoryDTO extends IRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @Nullable
    TaskDTO findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}