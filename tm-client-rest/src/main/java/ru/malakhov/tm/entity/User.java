package ru.malakhov.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumeration.Role;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private boolean locked = false;

    @NotNull
    private List<Project> projects= new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<Session> sessions = new ArrayList<>();

    public @NotNull String getLogin() {
        return login;
    }

    public @NotNull String getPasswordHash() {
        return passwordHash;
    }

    public @Nullable String getEmail() {
        return email;
    }

    public @Nullable String getFirstName() {
        return firstName;
    }

    public @Nullable String getLastName() {
        return lastName;
    }

    public @Nullable String getMiddleName() {
        return middleName;
    }

    public @NotNull Role getRole() {
        return role;
    }

    public boolean isLocked() {
        return locked;
    }

}