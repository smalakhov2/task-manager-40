package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}