package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.malakhov.tm.api.endpoint.IAuthRestEndpoint;

@RestController
@RequestMapping("/api/auth")
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;


    @Override
    @GetMapping
    public boolean login(
            @RequestParam("login") final String login,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    @Override
    @GetMapping("/logout")
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}
