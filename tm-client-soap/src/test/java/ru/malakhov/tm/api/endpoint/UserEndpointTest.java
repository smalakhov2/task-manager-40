package ru.malakhov.tm.api.endpoint;

import marker.UnitCategory;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.bootstrap.Bootstrap;

@Category(UnitCategory.class)
public class UserEndpointTest {

    private final Bootstrap bootstrap = new Bootstrap();
//    private Session userSession;
//
//    @Before
//    public void initSession() {
//        bootstrap.getUserEndpoint().create("user", "user");
//        userSession = bootstrap.getSessionEndpoint().open("user", "user");
//    }
//
//    @After
//    public void clearData() {
//        bootstrap.getAdminEndpoint().removeByLogin(bootstrap.getSessionEndpoint().open("admin", "admin"), "user");
//    }
//
//    @Test
//    public void createUserTest() {
//        final User user1 = bootstrap.getUserEndpoint().create("newUser", "pass");
//        final Session session = bootstrap.getSessionEndpoint().open("newUser", "pass");
//        Assert.assertEquals(user1.getLogin(), bootstrap.getUserEndpoint().profile(session).getLogin());
//        Assert.assertEquals(user1.getPasswordHash(), bootstrap.getUserEndpoint().profile(session).getPasswordHash());
//        bootstrap.getSessionEndpoint().close(session);
//        bootstrap.getAdminEndpoint().removeByLogin(bootstrap.getSessionEndpoint().open("admin", "admin"), "newUser");
//    }
//
//    @Test
//    public void profileTest() {
//        final User user1 = bootstrap.getUserEndpoint().create("newUser", "pass");
//        final Session session = bootstrap.getSessionEndpoint().open("newUser", "pass");
//        Assert.assertEquals(user1.getLogin(), bootstrap.getUserEndpoint().profile(session).getLogin());
//        Assert.assertEquals(user1.getPasswordHash(), bootstrap.getUserEndpoint().profile(session).getPasswordHash());
//        Assert.assertEquals(user1.getEmail(), bootstrap.getUserEndpoint().profile(session).getEmail());
//        Assert.assertEquals(user1.getFirstName(), bootstrap.getUserEndpoint().profile(session).getFirstName());
//        Assert.assertEquals(user1.getLastName(), bootstrap.getUserEndpoint().profile(session).getLastName());
//        Assert.assertEquals(user1.getMiddleName(), bootstrap.getUserEndpoint().profile(session).getMiddleName());
//        Assert.assertEquals(user1.getRole(), bootstrap.getUserEndpoint().profile(session).getRole());
//        bootstrap.getSessionEndpoint().close(session);
//        bootstrap.getAdminEndpoint().removeByLogin(bootstrap.getSessionEndpoint().open("admin", "admin"), "newUser");
//    }
//
//    @Test
//    public void changeEmailTest() {
//        bootstrap.getUserEndpoint().changeEmail(userSession, "email");
//        Assert.assertEquals(bootstrap.getUserEndpoint().profile(userSession).getEmail(), "email");
//    }
//
//    @Test
//    public void changePasswordTest() {
//        bootstrap.getUserEndpoint().changePassword(userSession, "user", "newPass");
//        Assert.assertNotNull(bootstrap.getSessionEndpoint().open("user", "newPass"));
//    }
//
//    @Test
//    public void changeLoginTest() {
//        bootstrap.getUserEndpoint().create("newUser", "pass");
//        final Session session = bootstrap.getSessionEndpoint().open("newUser", "pass");
//        bootstrap.getUserEndpoint().changeLogin(session, "user2");
//        Assert.assertNotNull(bootstrap.getSessionEndpoint().open("user2", "pass"));
//        bootstrap.getSessionEndpoint().close(session);
//        bootstrap.getAdminEndpoint().removeByLogin(bootstrap.getSessionEndpoint().open("admin", "admin"), "user2");
//    }
//
//    @Test
//    public void changeFirstName() {
//        bootstrap.getUserEndpoint().changeFirstName(userSession, "newName");
//        Assert.assertEquals(bootstrap.getUserEndpoint().profile(userSession).getFirstName(), "newName");
//    }
//
//    @Test
//    public void changeMiddleName() {
//        bootstrap.getUserEndpoint().changeMiddleName(userSession, "newName");
//        Assert.assertEquals(bootstrap.getUserEndpoint().profile(userSession).getMiddleName(), "newName");
//    }
//
//    @Test
//    public void changeLastName() {
//        bootstrap.getUserEndpoint().changeLastName(userSession, "newName");
//        Assert.assertEquals(bootstrap.getUserEndpoint().profile(userSession).getLastName(), "newName");
//    }

}
