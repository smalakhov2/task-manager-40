package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.endpoint.IProjectRestEndpoint;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.util.UserUtil;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @PostMapping("/create")
    public void create(@RequestBody String name) {
        projectService.create(UserUtil.getUserId(), name);
    }

    @Override
    @PutMapping
    public void update(@RequestBody ProjectDTO projectDTO) {
        projectService.updateDTO(UserUtil.getUserId(), projectDTO);
    }

    @Override
    @GetMapping("/${id}")
    public ProjectDTO findOneByIdDTO(@PathVariable("id") String id) {
        return projectService.findOneByIdDTO(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/exist/${id}")
    public boolean existsById(@PathVariable("id") String id) {
        return projectService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping("/${id}")
    public void deleteOneById(@PathVariable("id") String id) {
        projectService.removeOneById(UserUtil.getUserId(), id);
    }

}
