package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void updateDTO(@Nullable String userId, @Nullable ProjectDTO project);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<ProjectDTO> findAllDTOByUserId(@Nullable String userId);

    void deleteAll();

    void deleteAllByUserId(@Nullable String userId);

    @NotNull
    ProjectDTO findOneByIdDTO(@Nullable String userId, @Nullable String id);

    void removeOneById(@Nullable String userId, @Nullable String id);

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

}