package ru.malakhov.tm.dto.status;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

public class Result {

    public Boolean success = true;

    public String message = "";

    public Result(@NotNull Boolean success) {
        this.success = success;
    }

    public Result() {
    }

}
