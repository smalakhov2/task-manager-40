package ru.malakhov.tm.listener.task;

import org.springframework.beans.factory.annotation.Autowired;
import ru.malakhov.tm.endpoint.soap.TaskSoapEndpoint;
import ru.malakhov.tm.listener.AbstractListener;

public abstract class AbstractTaskListener extends AbstractListener {

    @Autowired
    protected TaskSoapEndpoint taskEndpoint;

}